from django.urls import path
from .views import recipe_list, show_recipe, create_recipe, edit, my_recipe_list

urlpatterns = [
    path("recipes/<int:id>/", show_recipe, name="show_recipe"),
    path("recipes/", recipe_list, name = "recipes"),
    path("recipes/create/", create_recipe, name="create_recipe"),
    path("recipes/<int:id>/edit", edit, name="edit"),
    path("mine/", my_recipe_list, name="my_recipe_list")
]
