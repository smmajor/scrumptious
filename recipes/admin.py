from django.contrib import admin
from recipes.models import Recipe, RecipeStep, Ingredient
# Register your models here.

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = ("title", "id", "description", "new_recipe",)

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = ("step_number", "id", "instruction",)

@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = ("amount", "food_item",)
